package service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.util.HibernateUtilActionWallet;
import model.ActionWallet;

public class AddingWalletService {

	public boolean addActionWallet(ActionWallet actionWallet){
		Session session = HibernateUtilActionWallet.openSession();
		
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.saveOrUpdate(actionWallet);
			tx.commit();
		} catch (Exception e) {
			if (tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return true;
	}
	
/*	public boolean countingCosts(ActionWallet actionWallet){
		Session session = HibernateUtilActionWallet.openSession();
		boolean result = false;
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query =  (Query) session.createQuery("select car from ActionWallet where id ='" + actionWallet.getId() +"'").list();
			System.out.println(actionWallet.getId());
			ActionWallet aW = (ActionWallet) query.uniqueResult();
			tx.commit();
			if (aW != null);
		} catch (Exception e){
			if (tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
		}
		*/
	}
	

