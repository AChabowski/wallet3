package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ACTIONWALLET_TABLE")
public class ActionWallet implements Serializable{

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Long id;
	private Long result;
	private Long car;
	private Long food;
	private Long entert;
	private Long other;
	
	
	public ActionWallet(){
		
	}

	public ActionWallet(Long result, Long car, Long food, Long entert, Long other) {
		this.result = result;
		this.car = car;
		this.food = food;
		this.entert = entert;
		this.other = other;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getResult() {
		return result;
	}

	public void setResult(Long result) {
		this.result = result;
	}

	public Long getCar() {
		return car;
	}

	public void setCar(Long car) {
		this.car = car;
	}

	public Long getFood() {
		return food;
	}

	public void setFood(Long food) {
		this.food = food;
	}

	public Long getEntert() {
		return entert;
	}

	public void setEntert(Long entert) {
		this.entert = entert;
	}

	public Long getOther() {
		return other;
	}

	public void setOther(Long other) {
		this.other = other;
	}
	
	
}
