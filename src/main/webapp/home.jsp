<%@page import="java.util.List"%>
<%@page import="service.LoginService"%>
<%@page import="java.util.Date"%>
<%@page import="service.NewWalletService"%>
<%@page import="model.User"%>
<%@page import="model.Wallet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/file.css" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
<title>Result Page</title>
</head>
<body>
	

	<div id="wrapper">
		<div id=container" >
		
				<%
				User user = (User) session.getAttribute("user");

				Wallet wallet = (Wallet) session.getAttribute("wallet");
				%>
			
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<ul class="nav navbar-left top-nav">
				<li>
				<h3><b>Welcome <%=user.getFirstName() + " " + user.getLastName()%></b></h3>
				</li>
			</ul>
		
			<ul class="nav navbar-right top-nav">
				<li>
					<a href="logout.jsp"><i class="fa fa-fw fa-power-off"></i>Log out</a>
				</li>
			</ul>
			
		</div>
		</div>
		
	<div class="row vertical-offset-100">	
		<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-default">
					  	<div class="panel-heading">
			    	<h3 class="panel-title">Menu</h3>
			 	</div>
		</div>
		
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                    <a href="newwallet.jsp"><i class="fa fa-credit-card-alt"></i> Add wallet</a>
          			</li>
          			<li>
                        <a href="listWallets.jsp"><i class="fa fa-fw fa-table"></i> List of wallets</a>
                    </li>
          	    </ul>
        
        </div>
    </div>    
         </nav>
	
		
		


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>



</body>
</html>
