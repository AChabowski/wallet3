<%@page import="java.util.List"%>
<%@page import="service.LoginService"%>
<%@page import="java.util.Date"%>
<%@page import="service.NewWalletService"%>
<%@page import="model.User"%>
<%@page import="model.Wallet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/file.css" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">


<title>Insert title here</title>
</head>
<body>


	<div id="wrapper">
		<div id=container">
		
		
				<%
				User user = (User) session.getAttribute("user");
				Wallet wallet = (Wallet) session.getAttribute("wallet");
				
				%>
			
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<ul class="nav navbar-left top-nav">
				<li>
				<h3><b>Welcome <%=user.getFirstName() + " " + user.getLastName()%></b></h3>
				</li>
			</ul>
		
			<ul class="nav navbar-right top-nav">
				<li>
					<a href="logout.jsp"><i class="fa fa-fw fa-power-off"></i>Log out</a>
				</li>
			</ul>
			
		</div>



			<div class="row vertical-offset-100">
				<div class="col-md-4 col-md-offset-4">
				    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">List of wallets</h3>
			 	</div>
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<%
						NewWalletService newWalletService = new NewWalletService();
						List<Wallet> list = newWalletService.getListOfWallets();
						for (Wallet w : list) {
					%>
					<tr>
						<td><%=w.getWalletName()%></td>
						<td><%=w.getWalletDesc()%></td>
						<td><a href="walletlogin.jsp?walletId=<%=w.getId()%>" class="btn btn-primary" role="button"><i class="fa fa-bar-chart" aria-hidden="true"></i> Edit</a></td>
					</tr>
					<%
						}
					%>
				
				<tbody>
			</table>
		</div>
		</div>
		</div>
		</div>

</body>
</html>