<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="model.Wallet"%>
<%@page import="java.util.List"%>
<%@page import="service.NewWalletService"%>
<%@page import="java.util.Date"%>
<%@page import="model.User"%>
<%@page import="model.ActionWallet" %>
<%@page import="service.AddingWalletService" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/file.css" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<title>Edit your wallet</title>
</head>
<body>     
	<form action="AddingWalletServlet" method="POST"> 
	<div id=container">
	
		<%
		ActionWallet actionWallet = (ActionWallet) session.getAttribute("actionWallet");
		%>
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				  <div class="panel panel-default">
				  
			  		<div class="panel-heading">
						<h3 class="panel-title">
				<%
 					 NewWalletService newWalletService = new NewWalletService();
				 	 Long id = Long.parseLong(request.getParameter("walletId"));
					 request.getSession().setAttribute("walletId", id);
					 List<Wallet> list = newWalletService.getWalletById(id);
					 for (Wallet w : list) {  
				 %>	
				 Wallet Name: <%=w.getWalletName() %>
						

						</h3>
					</div>
					
		<table class="table table-bordered table-condensed">
			<thead>
				<tr class="active">
				</tr>
			</thead>
		
			<tbody>
				<tr class="success">
					<td>Pensja</td>
					<td><%=w.getWalletBalance() + " PLN" %></td>
					<td></td> 
				</tr>
				<%}%>
				<tr class="danger">
					<td>Samochód</td>
					<td><input type="text" class="form-control" name="car"></input></td>	
				</tr>
				<tr class="warning">
					<td>Jedzenie</td>
					<td><input type="text" class="form-control" name="food"></input></td>
					</td>
				</tr>
				<tr class="info">
					<td>Rozrywka</td>
					<td><input type="text" class="form-control" name="entert"></input></td>
				</tr>
				<tr class="success">
					<td>Inne</td>
					<td><input type="text" class="form-control" name="other"></input></td>
				</tr>				
			</tbody>
		</table>

		<div class="login-register">
			<input class="btn btn-success btn-block" type="submit"value="Add changes">
		 
			<input class="btn btn-danger btn-block" type="reset" value="Remove">
		</div>
	</div>
</body>
</html>