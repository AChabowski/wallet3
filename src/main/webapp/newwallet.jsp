<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New wallet</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link type="text/css" href="./css/bootstrap.css" rel="stylesheet" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

</head>

<body>

	<form action="NewWalletServlet" method="POST">
		<div class="container">
			<div class="row vertical-offset-100">
				<div class="col-md-4 col-md-offset-4">
				    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">Create your wallet</h3>
			 	</div>
			 	<div class="panel-body">
					<fieldset>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span> 
								<input class="form-control" placeholder="Wallet Name" name="walletName" type="text">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text" aria-hidden="true"></i></span> 
								<input class="form-control" placeholder="Wallet Description" name="walletDesc" type="text">
							</div>
						</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span> 
									<input class="form-control" placeholder="Wallet Balance" name="walletBalance" type="text">
								</div>
							</div>
							<input class="btn btn-success btn-block" type="submit" value="Submit"> 
							<input class="btn btn-danger btn-block" type="reset" value="Reset">

						</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>

	</form>

</body>
</html>